$(function () {
    var isArchive = $('.root').hasClass('archive');

    function generateTask(id, description, depth) {
        return $('<div/>', {
            class: 'row',
            'data-key': id
        }).html(
            $('<div/>', {
                class: 'well well-sm col-sm-offset-' + depth + ' col-sm-' + (12 - depth),
                'data-depth': depth
            }).html(
                $('<span/>', {
                    class: 'lead'
                }).text(description)
            )
        );
    }

    var $modal = $('#editTask');

    $modal.on('shown.bs.modal', function () {
        $modal.find('textarea').focus();
    });

    $('.root').on('click', '.btn-delete', function (e) {
        e.preventDefault();
        var id = $(this).closest('.row').data('key');
        var $el = $(this);
        $.ajax({
            url: '/task/delete/' + id,
            type: 'POST',
            dataType: 'json'
        }).done(function (data) {
            if (data['status']) {
                for (var i = 0; i < data['delete'].length; i++) {
                    $('.row[data-key="' + data['delete'][i] + '"]').slideToggle('slow');
                }
            }
        });
    }).on('mouseenter', '.row[data-key]', function (e) {
        $(this).find('div').append(
            $('<span\>', {
                class: 'panel-action pull-right'
            }).html(
                isArchive ?
                    $('<i/>', {
                        class: 'glyphicon glyphicon-remove-circle btn-delete text-danger'
                    })
                    :
                    $('<i/>', {
                        class: 'glyphicon glyphicon-ok btn-change-status text-success'
                    }).add(
                        $('<i/>', {
                            class: 'glyphicon glyphicon-plus-sign btn-add text-success'
                        })
                    ).add(
                        $('<i/>', {
                            class: 'glyphicon glyphicon-remove-circle btn-delete text-danger'
                        })
                    )
            )
        );
    }).on('mouseleave', '.row[data-key]', function (e) {
        e.stopPropagation();
        var $button = $(this).find('.panel-action');
        if ($button.length) {
            $button.remove();
        }
    }).on('click', '.btn-add', function () {
        if (!isArchive) {
            var id = $(this).closest('.row').data('key');
            $modal.find('#task-description').val('');
            $modal.find('button').data('action', 'create').data('id', id ? id : 0);
            $modal.find('#task-parent_id').val(id);
            $modal.modal();
        }
    }).on('click', '.row[data-key] i.btn-change-status', function () {
        var id = $(this).closest('.row').data('key');
        $.ajax({
            url: '/task/complete/' + id,
            type: 'POST',
            dataType: 'json'
        }).done(function (data) {
            if (data['status']) {
                for (var i = 0; i < data['setCompleteId'].length; i++) {
                    $('.row[data-key="' + data['setCompleteId'][i] + '"]').css({'background-color': '#3c763d'}).slideToggle('slow');
                }
            }
        });
    }).on('dblclick', '.row[data-key]', function () {
        if (!isArchive) {
            var $modal = $('#editTask');
            $modal.find('#task-description').val($(this).closest('div').find('.lead').text());
            $modal.find('button').data('action', 'update').data('id', $(this).data('key'));
            $modal.modal();
        }
    });

    $('#taskForm').on('beforeSubmit', function (e) {
        if (!isArchive) {
            var $form = $(this);
            var $button = $form.find('button');
            var $targetRow = $('.row[data-key="' + $button.data('id') + '"]');
            var description = $targetRow.find('span').text();
            if ($button.data('action') == 'update') {
                if (description !== $form.find('textarea').val()) {
                    $.ajax({
                        url: '/task/' + $button.data('action') + '/' + $button.data('id'),
                        type: 'POST',
                        dataType: 'json',
                        data: $form.serialize()
                    }).done(function (data) {
                        if (data['status']) {
                            $targetRow.find('span').text(data['description']);
                            $modal.modal('hide');
                        } else {
                            alert(data['error']);
                        }
                    });
                } else {
                    $modal.modal('hide');
                }
            } else {
                $.ajax({
                    url: '/task/' + $button.data('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: $form.serialize()
                }).done(function (data) {
                    if (data['status']) {
                        var parentDepth = $targetRow.find('div').data('depth');
                        if (typeof parentDepth !== 'undefined') {
                            var insertAfterId = $targetRow.data('key');
                            var $nextRow = $targetRow;
                            while (parentDepth >= 0) {
                                $nextRow = $nextRow.next();
                                if ($nextRow.find('div').data('depth') > parentDepth) {
                                    insertAfterId = $nextRow.data('key');
                                } else {
                                    break;
                                }
                            }
                            if (insertAfterId) {
                                generateTask(data['task']['id'], data['task']['description'], parentDepth + 1).insertAfter(
                                    $('.row[data-key="' + insertAfterId + '"')
                                );
                            }
                        } else {
                            var generatedNode = generateTask(data['task']['id'], data['task']['description'], 0);
                            if ($('.root .row:last').length) {
                                generatedNode.insertAfter(
                                    $('.root .row:last')
                                );
                            } else {
                                $('.root').prepend(generatedNode);
                            }
                        }
                        $modal.modal('hide');
                    } else {
                        alert(data['error']);
                    }
                });
            }
        }
        return false;
    });
});