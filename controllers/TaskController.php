<?php

namespace app\controllers;

use Yii,
    app\models\Task,
    yii\base\InvalidParamException,
    yii\helpers\Json,
    yii\filters\AccessControl,
    yii\data\ActiveDataProvider;

class TaskController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'update', 'complete', 'create', 'archive'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Task::find()->byUser()->byRank()->byStatus(Task::STATUS_ACTIVE),
//            'pagination' => [
//                'pageSize' => 0,
//            ],
        ]);
        $isArchive = false;

        return $this->render('index', compact('dataProvider', 'isArchive'));
    }

    public function actionArchive()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Task::find()->byUser()->byRank()->byStatus(Task::STATUS_COMPLETE),
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);
        $isArchive = true;
        return $this->render('index', compact('dataProvider', 'isArchive'));
    }

    public function getErrorMessage($status, $message = '')
    {
        $returnData = [
            'status' => 0
        ];
        switch ($status) {
            case 303:
                $returnData['error'] = $message ? $message : 'You don\' have permission to delete this task';
                break;
            case 404:
                $returnData['error'] = $message ? $message : 'Task not found';
                break;
            case 500:
                $returnData['error'] = $message ? $message : 'Error while processing';
                break;
            case 401:
                $returnData['error'] = $message ? $message : 'Error in input text';
                break;
        }

        return $returnData;
    }

    public function getMessage($status = 1, $params = [])
    {
        $params['status'] = $status;

        return $params;
    }

    public function actionCreate()
    {
        $returnData = $this->getMessage(0);
        if ($data = Yii::$app->request->post('Task')) {
            $model = new Task();
            $transaction = $model->getDb()->beginTransaction();
            try {
                $model->attributes = $data;

                if ($model->save()) {
                    $transaction->commit();
                    $returnData = [
                        'status' => 1,
                        'task' => $model->toArray(['id', 'description'])
                    ];
                }
            } catch (\yii\base\Exception $e) {
                $transaction->rollBack();
                $returnData = $this->getErrorMessage(500);
            }
        } else {
            throw new InvalidParamException('Task model don\'t send');
        }
        echo Json::encode($returnData);
    }

    public function actionUpdate($id)
    {
        if ($data = Yii::$app->request->post('Task')) {
            $model = Task::findOne($id);
            if($model->hasOwn()){
                $model->attributes = $data;
                if ($model->update('description')) {
                    $returnData = $this->getMessage(1, ['description' => $model->description]);
                } else {
                    $returnData = $this->getErrorMessage(401);
                }
            } else {
                $data = $this->getErrorMessage(303);
            }
        } else {
            $returnData = $this->getErrorMessage(401);
        }
        echo Json::encode($returnData);
    }

    public function actionDelete($id)
    {
        $model = Task::findOne($id);

        if ($model) {
            if ($model->hasOwn()) {
                $transaction = $model->getDb()->beginTransaction();
                if($model->status == $model::STATUS_COMPLETE){
                    $result = $model->delete() ? [$model->id] : [];
                } else {
                    $result = $model->deleteChild();
                }
                if ($result) {
                    $transaction->commit();
                    $data = ['status' => 1, 'message' => 'Successfully deleted', 'delete' => $result];
                } else {
                    $transaction->rollBack();
                    $data = $this->getErrorMessage(500, 'Can\'t delete this task');
                }
            } else {
                $data = $this->getErrorMessage(303);
            }
        } else {
            $data = $this->getErrorMessage(404);
        }

        echo Json::encode($data);
    }

    public function actionComplete($id)
    {
        $model = Task::findOne($id);
        if($model->hasOwn()){
            $changedIds = [];
            $transaction = $model->getDb()->beginTransaction();
            if ($children = $model->getChildren()->byStatus($model::STATUS_ACTIVE)->asArray()->all()) {
                $changedIds = array_column($children, 'id');
            }
            $changedIds[] = $model->id;
            $changedIds = $model->recountStatusChange($changedIds);
            if ($changedIds) {
                Task::updateAll(['status' => Task::STATUS_COMPLETE], ['id' => $changedIds]);
            }
            $transaction->commit();
            $returnData = $this->getMessage(1, ['setCompleteId' => $changedIds]);
        } else {
            $returnData = $this->getErrorMessage(303);
        }
        echo Json::encode($returnData);
    }
}
