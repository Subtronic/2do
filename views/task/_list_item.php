<?php
 $positionClass = $isArchive ? 'col-sm-12' : 'col-sm-offset-'.$model->depth.' col-sm-'.(12 - $model->depth);
?>
<div
    data-depth="<?=$model->depth?>"
    class="well well-sm <?=$positionClass?>">
    <span class="lead"><?= $model->description ?></span>
</div>