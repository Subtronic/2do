<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string                       $isArchive
 */
use yii\helpers\Html,
    yii\widgets\ListView;

$this->title = $isArchive ? 'Archive todo list' : 'Todo list';
$this->params['breadcrumbs'][] = $this->title; ?>

    <h1><?= Html::encode($this->title) ?>:</h1>
    <br>
    <div class="body-content">
<?php
$addButton = $isArchive ? '' : '<div class="col-sm-12 well-sm btn btn-success btn-add"><i class="glyphicon glyphicon-plus"></i></div>';
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'tag' => 'div',
        'class' => 'root' . ($isArchive ? ' archive' : ''),
        'id' => 'task-list-wrapper',
    ],
    'itemOptions' => [
        'class' => 'row',
    ],
    'emptyText' => $isArchive ? 'Your archive is empty' : $addButton,
    'layout' => '{items}' . $addButton,
    'itemView' => '_list_item',
    'viewParams' => compact('isArchive'),
]);
echo $this->render('_modal_form');

