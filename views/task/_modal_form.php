<?php
use app\models\Task,
    yii\bootstrap\Modal,
    yii\bootstrap\Html,
    yii\widgets\ActiveForm;
//$this->menu = [['fdsfsd' => 'fsfsd']];
Modal::begin([
    'header' => 'Task modal',
    'id' => 'editTask',
    'class' => 'modal',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

$model = new Task();
$form = ActiveForm::begin(['id' => 'taskForm']);
echo $form->field($model, 'description')->textarea(['rows' => 6]);
echo $form->field($model, 'parent_id')->hiddenInput()->label(false);
echo Html::submitButton('Save', ['id' => 'modalSubmit', 'class' => 'btn-primary']);
ActiveForm::end();
Modal::end();