<?php

/* @var $this yii\web\View */
$this->title = '2do application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>2do</h1>
        <p class="lead">Hold here your todo list </p>

        <!--        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>-->
        <?php if (Yii::$app->user->isGuest): ?>
            <div>
                <a class="btn btn-lg btn-success" href="/user/register">Sign up</a>
                <a class="btn btn-lg btn-success" href="/user/login">Sign in</a>
            </div>

        <?php else: ?>
            <p><a class="btn btn-lg btn-info" href="/task/">To my lists</a></p>
        <?php endif; ?>
    </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Using to store</h2>
                <p>To store we using Path Enumeration algorithm</p>
                <p><a class="btn btn-default" href="http://www.slideshare.net/billkarwin/models-for-hierarchical-data">Models for hierarchical data &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Using framewok</h2>

                <p>We use Yii2 framework with basic-template</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/">Yii site &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Technologies</h2>
                <p>PHP 5.6, MySQL (percona-server 5.7.11-4)</p>
            </div>
        </div>

    </div>
</div>
