2do list for Cognitive technologies
============================
Here example of application for 2do list.


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:


```
#!php

return [
    'dsn' => 'mysql:host=localhost;dbname=todo',
    'username' => 'todo',
    'password' => 'todo',
];
```


TO INSTALL
==========


```
#!bash

composer install
php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
php yii migrate/up
```