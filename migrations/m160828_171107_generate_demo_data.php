<?php

use yii\db\Migration;

class m160828_171107_generate_demo_data extends Migration
{
    public function safeUp()
    {
        function addTask(array $data, Migration $migration)
        {
            $migration->insert('{{%task}}', [
                'description' => $data['desc'],
                'user_id' => $data['user_id'],
            ]);
            $lastTaskId = Yii::$app->db->getLastInsertID();
            $migration->update('{{%task}}', ['rank' => $data['rank'] . '/' . $lastTaskId], ['id' => $lastTaskId]);

            return $lastTaskId;
        }

        $this->insert('{{%user}}', [
            'username' => 'admin',
            'email' => 'crashmybrain@gmail.com',
            'password_hash' => '$2y$12$/fdJXZMkoRHn4H9/HDInLOt8fvKkZm/zFuVdmZ0ZVH8CH0h7jUblm',
            'auth_key' => 'qKISmiOKhBRVutc5xmvNAcnxS9HNnYoE',
            'confirmed_at' => 1470833950,
            'unconfirmed_email' => null,
            'blocked_at' => null,
            'registration_ip' => '127.0.0.1',
            'created_at' => 1470833930,
            'updated_at' => 1472404566,
            'flags' => 0

        ]);
        $user_id = Yii::$app->db->getLastInsertID();
        $this->insert('{{%profile}}', [
            'user_id' => $user_id,
        ]);

        $this->insert('{{%task}}', [
            'description' => '1',
            'user_id' => $user_id,
        ]);
        $lastTaskId = Yii::$app->db->getLastInsertID();
        $this->update('{{%task}}', ['rank' => $lastTaskId], ['id' => $lastTaskId]);

        $rank = $lastTaskId;
        $lastTaskId = addTask(['rank' => $rank, 'desc' => '1/1', 'user_id' => $user_id],$this);
        $lastTaskId = addTask(['rank' => $rank, 'desc' => '1/2', 'user_id' => $user_id],$this);
        $lastTaskId = addTask(['rank' => $rank, 'desc' => '1/3', 'user_id' => $user_id],$this);
        $rank = $rank . '/' . $lastTaskId;
        $lastTaskId = addTask(['rank' => $rank, 'desc' => ' 1/3/1', 'user_id' => $user_id],$this);
        $lastTaskId = addTask(['rank' => $rank, 'desc' => ' 1/3/2', 'user_id' => $user_id],$this);
        $lastTaskId = addTask(['rank' => $rank, 'desc' => ' 1/3/3', 'user_id' => $user_id],$this);
        $rank = $rank . '/' . $lastTaskId;
        $lastTaskId = addTask(['rank' => $rank, 'desc' => ' 1/3/3/1', 'user_id' => $user_id],$this);
    }

    public function safeDown()
    {
        echo "m160828_171107_generate_demo_data cannot be reverted.\n";
        $this->truncateTable('{{%task}}');
        $this->delete('{{%user}}',['username' => 'admin']);
//        $this->truncateTable('{{%user}}');
//        $this->truncateTable('{{%profile}}');
        return true;
    }
}
