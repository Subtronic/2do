<?php

use yii\db\Migration,
    yii\db\Schema;

class m160828_145406_init_task extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%task}}', [
            'id' => Schema::TYPE_PK,
            'description' => Schema::TYPE_TEXT,
            'user_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'rank' => Schema::TYPE_STRING . '(255) NOT NULL',
        ]);

        $this->createIndex('task_status', '{{%task}}', 'status');
        $this->createIndex('unique_rank', '{{%task}}', 'rank', true);
        $this->createIndex('user_id_key', '{{%task}}', 'user_id');
        $this->addForeignKey('task_user_fk', '{{%task}}', 'user_id', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        echo "Reverting m160828_145406_init_task. \n";
        $this->dropForeignKey('task_user_fk', '{{%task}}');
        $this->dropTable('{{%task}}');
    }
}
