<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Task]].
 *
 * @see Task
 */
class TaskQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Task[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Task|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byUser()
    {
        return $this->andWhere(['user_id' => \Yii::$app->user->id]);
    }

    public function byRank()
    {
        return $this->orderBy(['rank' => SORT_ASC]);
    }

    public function byStatus($status)
    {
        return $this->andWhere('status = :state',[':state' => $status]);
    }

}
