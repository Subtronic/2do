<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string  $description
 * @property integer $user_id
 * @property string  $date_create
 * @property string  $date_update
 * @property integer $is_acrhived
 * @property integer $status
 *
 * @property Task[]  $descendants
 * @property Task[]  $ancestors
 */
class Task extends \yii\db\ActiveRecord
{
    public $parent_id = null;
    const STATUS_ACTIVE = 0;
    const STATUS_COMPLETE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required', 'message' => 'Please fill task text.'],
            [['description', 'rank'], 'string'],
            [['user_id', 'status', 'parent_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Task description',
            'user_id' => 'User',
            'status' => 'Complete',
//            'depth' => 'Глубина вложенности'
        ];
    }

    public function getActive()
    {

    }

    /**
     * @inheritdoc
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

    public function getParent()
    {
        return $this->find()->andWhere("rank = :rank", [':rank' => $this->parentRank]);
    }

    public function getBrothers()
    {
        return $this->find()->andWhere("rank REGEXP :rank AND id != :id", [':id' => $this->id, ':rank' => '^' . $this->parentRank . '/[0-9]+$']);
    }

    public function getChildren()
    {
        return $this->find()->andWhere("rank LIKE :rank", [':rank' => $this->rank . '/%']);
    }

    public function hasOwn()
    {
        return $this->user_id == Yii::$app->user->id; // add or is admin
    }

    public function deleteChild()
    {
        if($this->hasOwn()){
            $ids = array_reduce(
                self::find()->select('id')->where("rank LIKE :rank", [':rank' => $this->rank . '%'])->asArray()->all(),
                function ($carry, $item) {
                    $carry[] = (int)$item['id'];

                    return $carry;
                }, []);
            if ($ids) {
                if (self::deleteAll(['id' => $ids])) {
                    return $ids;
                }
            }
        }
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->user_id = \Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            if ($this->parent_id) {
                $parent = self::findOne($this->parent_id);
                $this->rank = $parent->rank . '/' . $this->id;
            } else {
                $this->rank = "$this->id";
            }
            if (!$this->save()) {
                Yii::trace('Can\'t save rank for new element', __METHOD__);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function recountStatusChange($changedIds)
    {
        return array_unique($this->recursiveRecountStatusChange($changedIds));
    }

    public function recursiveRecountStatusChange($changedIds)
    {
        if ($this->depth !== 0) {
            $brothers = $this->getBrothers()->byStatus($this::STATUS_ACTIVE)->all();
            $allChecked = true;
            $changedIdsTemp = [];
            foreach ($brothers as $brother) {
                if ($brother->status == self::STATUS_ACTIVE) {
                    $allChecked = false;
                    break;
                } else {
                    $changedIdsTemp[] = $brother->id;
                }
            }
            if ($allChecked) {
                // go to up on hierarchy
                $changedIds = array_merge($changedIds, $changedIdsTemp);
                $parent = $this->getParent()->one();
                if ($parent) {
                    $changedIds[] = $parent->id;

                    return $parent->recountStatusChange($changedIds);
                }
            }
        } else {
            $changedIds[] = $this->id;
        }

        return $changedIds;
        //recount after delete or update (update status)
    }

    public function getParentRank()
    {
        // @todo too much dirty!!!!!!
        return preg_replace('/\/' . $this->id . '$/', '', $this->rank);
    }


    public function getDepth()
    {
        return substr_count($this->rank, '/');
    }

//    public function switchToComplete()
//    {
//        $this->status = self::STATUS_COMPLETE;
//        $this->save();
//    }
}
